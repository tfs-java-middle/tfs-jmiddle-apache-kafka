package ru.tcsbank.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class MessageProcessorService {
    private static final String KAFKA_TOPIC = "demo.tinkoff";

    private static final Logger log = LoggerFactory.getLogger(MessageProcessorService.class);

    @KafkaListener(topics = KAFKA_TOPIC)
    public void processMessage(String message) {
        log.info("Message received: {}", message);
    }
}