package ru.tcsbank.service;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
public class MessageSendService {
    private static final String KAFKA_TOPIC = "demo.tinkoff";

    private static final Logger log = LoggerFactory.getLogger(MessageSendService.class);

    private final KafkaTemplate<String, String> kafkaProducer;

    @Autowired
    public MessageSendService(KafkaTemplate<String, String> kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    public void sendMessage(Integer number) {
        var key = "key-" + number;
        var value = "value-" + number;
        var producerRecord = new ProducerRecord<>(KAFKA_TOPIC, key, value);

        var listenableFuture = kafkaProducer.send(producerRecord);

        listenableFuture.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error("Failed to send message.", ex);
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                log.info("Partition %d. Offset %d".formatted(result.getRecordMetadata().partition(),
                        result.getRecordMetadata().offset()));
            }
        });
    }
}
