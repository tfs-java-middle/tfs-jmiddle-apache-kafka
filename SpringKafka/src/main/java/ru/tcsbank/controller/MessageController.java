package ru.tcsbank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.tcsbank.service.MessageSendService;

@RestController
public class MessageController {

    private final MessageSendService messageService;

    @Autowired
    public MessageController(MessageSendService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/{number}")
    public void sendMessage(@PathVariable("number") Integer number) {
        messageService.sendMessage(number);
    }
}
