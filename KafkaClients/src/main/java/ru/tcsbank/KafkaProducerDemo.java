package ru.tcsbank;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.HashMap;

public class KafkaProducerDemo {

    public static void main(String[] args) {
        // Подготавливаем конфигурацию
        var configuration = new HashMap<String, Object>();
        configuration.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // configuration.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        // configuration.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configuration.put(ProducerConfig.ACKS_CONFIG, "all");

        StringSerializer serializer = new StringSerializer();

        // Создаем producer
        try (var kafkaProducer = new KafkaProducer<>(configuration, serializer, serializer)) {

            // Закидываем сообщения в топик
            var topic = "demo.tinkoff";
            for (int i = 0; i < 4; i++) {
                var key = "key-" + i;
                var value = "value-" + i;
                var message = new ProducerRecord<>(topic, key, value);
                kafkaProducer.send(message);
            }
        }
    }

}
