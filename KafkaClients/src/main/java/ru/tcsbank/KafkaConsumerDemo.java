package ru.tcsbank;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;

@SuppressWarnings("InfiniteLoopStatement")
public class KafkaConsumerDemo {

    public static void main(String[] args) {
        var configuration = new HashMap<String, Object>();
        configuration.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        configuration.put(ConsumerConfig.GROUP_ID_CONFIG, "demo.group.java.consumer");
        configuration.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        configuration.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);


        try(var kafkaConsumer = new KafkaConsumer<>(configuration)) {
            kafkaConsumer.subscribe(Collections.singleton("demo.tinkoff"));
            while (true) {
                var records = kafkaConsumer.poll(Duration.ZERO);
                records.forEach(record -> System.out.printf("Key %s. Value %s.%n", record.key(), record.value()));
            }
        }
    }
}
